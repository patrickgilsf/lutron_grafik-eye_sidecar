--fader---------------------------------------------------------
 -- Main variables
 fIid = Controls.fIidNum.String
 fOutput = Controls.fOutNum.String
 
 --Fader control
 Controls.Fader.EventHandler = function()
   if Controls.Fader.Value ~= nil then
     FaderPos = string.sub(Controls.Fader.Value,0,4)
     print(FaderPos)
     FaderTx()
   end
   Timer.CallAfter(Faderwait,0.5)
 end
 Faderwait = function()
   if FaderPos == Controls.Fader.Value then
     FaderTx()
     FaderPos = string.sub(Controls.Fader.Value,0,4)
     FaderRx()
   end
 end
 
 --Transmit/Receive from 'CodeBase' script
 ExtRx = Component.New("Lutron GRAFIK_EYE Sidecar "..PluginInfo.Version)["ExtRead"]
 ExtTx = Component.New("Lutron GRAFIK_EYE Sidecar "..PluginInfo.Version)["ExtWrite"]
 FaderTx = function()
   ExtRx.String = '#DEVICE,'..fIid..','..fOutput..',14,'..FaderPos..'\x0d\x0a'
 end
 FaderRx = function()
   if ExtTx.String ~= nil then -- this is the one to run if you are not online
   --if string.find(ExtTx.String,'~DEVICE,'..fIid..','..fOutput..',14') then;
     Controls.FaderFb.String =  ExtTx.String else
     Controls.FaderFb.String = ""
   end
 end
 FaderRx()
 
 --Updates text boxes with number change
 Controls.fOutNum.EventHandler = function() 
   if Controls.fOutNum.String ~= nil then
     fOutput = Controls.fOutNum.String
   end
   print("Fader Output has changed to "..fOutput)
 end
 Controls.fIidNum.EventHandler = function() 
   if fIid ~= nil then
    fIid = Controls.IidNum.String
   end
   print("Fader Iid has changed to "..fIid)
 end    


--switch---------------------------------------------------------
  --Main variables
sIid = Controls.sIidNum.String
sOutput = Controls.sOutNum.String

--Timers
UpTimer = Timer.New()
DownTimer = Timer.New()

function TimerUp()
  Controls.SwUp.Boolean = false
  Controls.SwUp.Color = "white"
  Controls.SwDown.Color = "black"
  Controls.SwUp.Legend = "The Shades are Up"
  Controls.SwDown.Legend = ""
  UpTimer:Stop()
  print('UpTimer stopped')
  SwitchRx()
end

function TimerDown()
  Controls.SwDown.Boolean = false
  Controls.SwDown.Color = "white"
  Controls.SwUp.Color = "black"
  Controls.SwDown.Legend = "The Shades are Down"
  Controls.SwUp.Legend = ""
  DownTimer:Stop()
  print('DownTimer stopped')
  SwitchRx()
end

UpTimer.EventHandler = TimerUp
DownTimer.EventHandler = TimerDown

--Transmit/Receive from 'CodeBase' script
--ExtRx = Component.New("CodeBase")["ExtRead"]
--ExtTx = Component.New("CodeBase")["ExtWrite"]
SwitchUpTx = function()
  if Controls.SwDown.Boolean == false then
    ExtRx.String = '#DEVICE,'..sIid..','..sOutput..',14,100.00\x0d\x0a'
    SwitchRx()
    else ShadeStop()
    end
end
SwitchDownTx = function()
  if Controls.SwUp.Boolean == false then
  ExtRx.String = '#DEVICE,'..sIid..','..sOutput..',14,0.00\x0d\x0a'
  SwitchRx()
  else ShadeStop()
  end
end
ShadeStop = function()
  ExtRx.String = '#DEVICE,'..sIid..','..sOutput..',20,0.00\x0d\x0a'
  Controls.SwUp.Legend = "Shades Stopped"
  Controls.SwDown.Legend = "Shades Stopped"
  UpTimer:Stop()
  DownTimer:Stop()
  Controls.SwUp.Boolean = false
  Controls.SwDown.Boolean = false
  print('shades stopped')
  SwitchRx()
  end
SwitchRx = function()
  --if ExtTx.String ~= nil then -- this is the one to run if you are not online
  if ExtTx.String ~= nil and string.find(ExtTx.String,'~DEVICE,'..sIid..','..sOutput..',14') then
    Controls.SwFb.String =  ExtTx.String else
    Controls.SwFb.String = ""
  end
end
SwitchRx()

--Switch EventHandlers
Controls.SwUp.Boolean = false
Controls.SwDown.Boolean = false
print(Controls.SwUp.Boolean)
print(Controls.SwDown.Boolean)

Controls.SwUp.EventHandler = function()
  if Controls.SwUp.Boolean and Controls.SwDown.Boolean == false then
    SwitchUpTx()
    UpTimer:Start(5) -- find right amount of time to put in here when Nick is on site next about 24 seconds
    DownTimer:Stop()
    Controls.SwUp.Legend = "The Shades are Moving Up"
    Controls.SwDown.Legend = "The Shades are Moving"    
  else if Controls.SwUp.Boolean and Controls.SwDown.Boolean then
    ShadeStop()
  else if Controls.SwUp.Boolean == false then
    ShadeStop()
    end
  end
end
end
Controls.SwDown.EventHandler = function()
  if Controls.SwDown.Boolean and Controls.SwUp.Boolean == false then
    SwitchDownTx()
    DownTimer:Start(5)
    UpTimer:Stop()
    Controls.SwDown.Legend = "The Shades are Moving Down"
    Controls.SwUp.Legend = "The Shades are Moving"
  else if Controls.SwDown.Boolean and Controls.SwUp.Boolean then
    ShadeStop()
  else if Controls.SwDown.Boolean == false then
    ShadeStop()
    end
  end
end
end

--Updates text boxes with number change
Controls.sOutNum.EventHandler = function() 
  if Controls.sOutNum.String ~= nil then
    sOutput = Controls.sOutNum.String
  end
  print("sOutput has changed to "..sOutput)
end
Controls.sIidNum.EventHandler = function() 
  if Controls.sIidNum.String ~= nil then
    sIid = Controls.sIidNum.String
  end
  print("Switch Iid has changed to "..sIid)
end    


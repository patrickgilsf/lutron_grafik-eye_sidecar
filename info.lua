PluginInfo = {
  Name = "Lutron QSE-NWK-E Sidecar",
  Version = "1.0",
  BuildVersion = "1.0.19.0",
  Id = "b9947f69-a1c4-41cf-90ff-64cb736d15db",
  Author = "PatrickGilligan",
  Description = "Adds copy paste-able control extension to main plugin",
  ShowDebug = false  
}
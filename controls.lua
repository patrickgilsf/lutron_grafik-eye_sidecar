table.insert(ctrls, {
  Name = "Fader",
  ControlType = "Knob",
  ControlUnit = "Percent",
  Max = 100,
  Min = 0,
  Count = 1,
  UserPin = false
})
table.insert(ctrls, {
  Name = "fIidNum",
  ControlType = "Text",
  Count = 1,
  UserPin = false
})
table.insert(ctrls, {
  Name = "fOutNum",
  ControlType = "Text",
  Count = 1,
  UserPin = false
})
table.insert(ctrls, {
  Name = "FaderFB",
  ControlType = "Text",
  Count = 1,
  UserPin = false
})
table.insert(ctrls, {
  Name = "SwFB",
  ControlType = "Text",
  Count = 1,
  UserPin = false
})
table.insert(ctrls, {
  Name = "SwUp",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  UserPin = false
})
table.insert(ctrls, {
  Name = "SwDown",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  UserPin = false
})
table.insert(ctrls, {
  Name = "sIidNum",
  ControlType = "Text",
  Count = 1,
  UserPin = false
})
table.insert(ctrls, {
  Name = "sOutNum",
  ControlType = "Text",
  Count = 1,
  UserPin = false
})
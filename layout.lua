--this is the big group box
table.insert(graphics, {
  Type = "GroupBox",
  Test = "Lutron GRAFIK Eye Sidecar",
  Fill = {105,105,105},
  StrokeWidth = 1,
  Position = {1,1},
  Size = {653,165},
  HTextAlign = "Center"
})

--These are the smaller group boxes
table.insert(graphics,{
  Type = "GroupBox",
  Text = "Lighting Fader",
  Fill = {157,156,156},
  StrokeWidth = 1,
  Position = {8,30},
  Size = {318,129},
  HTextAlign = "Center"
})
table.insert(graphics,{
  Type = "GroupBox",
  Text = "Shade Switch",
  Fill = {157,156,156},
  StrokeWidth = 1,
  Position = {326,30},
  Size = {320,129},
  HTextAlign = "Center"
})

--Text boxes
table.insert(graphics,{
  Type = "Text",
  Text = "Integration ID",
  Position = {94,55},
  Size = {82,29},
  FontSize = 12,
  HTextAlign = "Center"
})
table.insert(graphics,{
  Type = "Text",
  Text = "Output Number",
  Position = {94,98},
  Size = {96,29},
  FontSize = 12,
  HTextAlign = "Center"
})
table.insert(graphics,{
  Type = "Text",
  Text = "Feedback",
  Position = {219,55},
  Size = {89,24},
  FontSize = 12,
  HTextAlign = "Center"
})
table.insert(graphics,{
  Type = "Text",
  Text = "Feedback",
  Position = {345,55},
  Size = {89,24},
  FontSize = 12,
  HTextAlign = "Center"
})
table.insert(graphics,{
  Type = "Text",
  Text = "Up",
  Position = {458,63},
  Size = {82,15},
  FontSize = 12,
  HTextAlign = "Center"
})
table.insert(graphics,{
  Type = "Text",
  Text = "Down",
  Position = {458,102},
  Size = {82,15},
  FontSize = 12,
  HTextAlign = "Center"
})
table.insert(graphics,{
  Type = "Text",
  Text = "Integration ID",
  Position = {555,55},
  Size = {82,29},
  FontSize = 12,
  HTextAlign = "Center"
})
table.insert(graphics,{
  Type = "Text",
  Text = "Output",
  Position = {543,98},
  Size = {96,29},
  FontSize = 12,
  HTextAlign = "Center"
})

--Controls
layout["Fader"] = {
  PrettyName = "Fader",
  Style = "Fader",
  FaderStyle = "Classic",
  Position = {27,30},
  Size = {36,108},
  Color = {242,137,174}
}
layout["fIidNum"] = {
  PrettyName = "Integration ID",
  Style = "TextBox",
  Position = {94,84},
  Size = {80,14},
  Color = {255,255,255}
}
layout["fOutNum"] = {
  PrettyName = "Output",
  Style = "TextBox",
  Position = {94,127},
  Size = {80,14},
  Color = {255,255,255}
}
layout["FaderFB"] = {
  PrettyName = "Feedback",
  Style = "TextBox",
  Position = {212,79},
  Size = {103,66},
  Color = {255,255,255}
}
layout["SwFB"] = {
  PrettyName = "Feedback",
  Style = "TextBox",
  Position = {339,79},
  Size = {103,66},
  Color = {255,255,255}
}
layout["SwUp"] = {
  PrettyName = "Up",
  Style = "Button",
  Position = {458,78},
  Size = {78,24},
  Color = {255,255,255}
}
layout["SwDown"] = {
  PrettyName = "Up",
  Style = "Button",
  Position = {458,121},
  Size = {78,24},
  Color = {255,255,255}
}
layout["sIidNum"] = {
  PrettyName = "Integration ID",
  Style = "TextBox",
  Position = {555,84},
  Size = {74,14},
  Color = {255,255,255}
}
layout["sOutNum"] = {
  PrettyName = "Output",
  Style = "TextBox",
  Position = {557,127},
  Size = {74,14},
  Color = {255,255,255}
}